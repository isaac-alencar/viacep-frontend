# Projeto Via Cep - Front-End

<br />
	<div style="display:flex;justify-content: center; flex-direction: column; gap: 10px">
		<img src="https://i.imgur.com/JnSvj3U.png" style="margin:0 auto;" alt="homepage" width="70%" object-fit="cover" />
		<br />
		<img src="https://i.imgur.com/eTpdvL6.png" style="margin:0 auto;" alt="homepage"  width="70%" object-fit="cover" />
	</div>
<br />

### Sobre - visão geral do projeto
O projeto via cep consiste em uma aplicação web que busca informações sobre um dado cep.  
Esse projeto é parte do desafio referente a vaga na empresaa Eureka.

###  Tecnologias Utilizadas
 - **ReactJS** - biblioteca de construção de intefaces de usuário para web;
 - **Axios** -  biblioteca que disponibiliza um client http para consultas em api;
 - **React Testing-Library** - biblioteca de testes voltada para react.
 
### Como Executar o Projeto
- Primeiro clone o repositório em sua máquina

- Depois disso, mova para o diretório do projeto e abra no seu editor de código; 

- Em seguida, no terminal, execute:

```bash

yarn

# ou caso você prefira usar npm

npm install # ou npm i

```

Depois que todas as dependências estiverem instaladas, execute:

```bash

yarn start

# ou, caso use npm

npm start # ou npm run start

```

### Como Executar os Testes

- Vá novamente ao terminal na pasta do projeto e execute:

```bash

yarn test

# ou 

npm test # ou npm t

```
