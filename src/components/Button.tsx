import { ButtonHTMLAttributes } from "react";

import "../styles/button.css";

type ButtonProps = ButtonHTMLAttributes<HTMLElement> & {
  label: string;
  variant?: boolean;
};

function Button({ label, variant, ...props }: ButtonProps) {
  return (
    <button
      style={{
        backgroundColor: variant
          ? "var(--button-green)"
          : "var(--button-color)",
      }}
      className="button"
      type="submit"
      {...props}
    >
      {label}
    </button>
  );
}

export { Button };
