import { InputHTMLAttributes } from "react";

import '../styles/input.css';

function Input(props: InputHTMLAttributes<HTMLInputElement>) {
  return <input aria-label="zipcode-input" className="input" {...props} />
}

export { Input };