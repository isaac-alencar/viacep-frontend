import { render, screen } from "@testing-library/react";
import App from "../App";

describe("<App />", () => {
  it("should render application without crashing", () => {
    render(<App />);

    expect(screen.getByText(/Buscar por um cep/)).toBeInTheDocument();
  });
});
