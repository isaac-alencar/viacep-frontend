import { render, screen, fireEvent } from "@testing-library/react";
import { Button } from "../../components/Button";

describe("<Button />", () => {
  it("should render button correctly with a given label", () => {
    render(<Button label="Test" />);

    const button = screen.getByRole("button", { name: "Test" });

    expect(button).toBeInTheDocument();
    expect(button).toHaveClass("button");
  });

  it("should click button once without errors", () => {
    const handleClick = jest.fn(() => render(<h1>Button Clicked</h1>));

    render(<Button label="Test" onClick={handleClick} />);

    const button = screen.getByRole("button", { name: "Test" });
    fireEvent.click(button);

    expect(handleClick).toHaveBeenCalledTimes(1);
    expect(screen.getByRole("heading")).toBeInTheDocument();
  });
});
