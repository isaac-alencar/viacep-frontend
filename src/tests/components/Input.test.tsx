import { render, screen, fireEvent } from "@testing-library/react";
import { Input } from "../../components/Input";

describe("<Input />", () => {
  it("should render input correctly", () => {
    render(<Input placeholder="Ex: 00000-000" />);

    const inputElement = screen.getByLabelText(
      "zipcode-input"
    )
    expect(inputElement).toHaveAttribute("placeholder", "Ex: 00000-000");
  });

  it("should display value typed on input", () => {
    render(<Input />);

    const inputElement = screen.getByLabelText(
      "zipcode-input"
    ) as HTMLInputElement;
    fireEvent.change(inputElement, { target: { value: "00000-000" } });

    expect(inputElement.value).toBe("00000-000");
  });
});
