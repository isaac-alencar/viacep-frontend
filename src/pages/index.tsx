import { ChangeEvent, useState } from "react";

import { api } from "../services/api";

import { Button } from "../components/Button";
import { Input } from "../components/Input";

import "../styles/home_page.css";

export function Home() {
  const [zipcode, setZipCode] = useState<string>("");

  const [data, setData] = useState<any | undefined>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const [isButtonVisible, setIsButtonVisible] = useState<boolean>();

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setZipCode(e.target.value);
  };

  const handleClick = async () => {
    const endpoint = `/address/${zipcode}`;

    setError("");
    setLoading(true);
    setData(undefined);
    
    try {
      const response = await api.get(endpoint);

      if(response.data) {
        setLoading(false);
        setData(response.data);
        setError("");
      }
      
      setIsButtonVisible(true);

    } catch (error) {
      setLoading(false);
      setError("Ops, verifique o cep que você digitou");
    }

    return;
  };

  const handleNewSearch = () => {
    setZipCode("");
    setData(undefined);
    setIsButtonVisible(false);

    return;
  }


  return (
    <div className="container">
      <div className="form">
        <h2>Buscar por um cep</h2>
        <Input
          value={zipcode}
          onChange={(e) => handleInputChange(e)}
          type="text"
          placeholder="Ex: 00000-000"
          required
        />
        <div className="buttons">
          <Button label="Buscar" type="button" onClick={handleClick} />
          {isButtonVisible ? (
            <Button
              variant
              label="Nova Busca"
              type="button"
              onClick={handleNewSearch}
            />
          ) : (
            <></>
          )}
        </div>
      </div>

      {loading && <h2>Caregando...</h2>}
      {error && <h2>{error}</h2>}

      {data && (
        <div className="result">
          <h2>Cep {data?.zipcode}</h2>
          <div className="content">
            <strong>
              Logradouro: <span>{data?.street}</span>
            </strong>
            <strong>
              Bairro: <span>{data?.district}</span>
            </strong>
            <strong>
              Cidade: <span>{data?.city}</span>
            </strong>
            <strong>
              Uf: <span>{data?.uf}</span>
            </strong>
          </div>
        </div>
      )}
    </div>
  );
}
